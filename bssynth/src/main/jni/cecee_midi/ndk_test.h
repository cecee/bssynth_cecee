//
// Created by thpark on 2020-11-05.
//
#include <string.h>
#include <stdio.h>
#include <string>
using namespace std;

#ifndef BSSYNTH_SAMPLE_NDK_TEST_H
#define BSSYNTH_SAMPLE_NDK_TEST_H


class ndk_test {
public:
    string version;
    ndk_test();
    ~ndk_test();
    void ndk_string(void);
};


#endif //BSSYNTH_SAMPLE_NDK_TEST_H
